package cz.janousek.adventofcode2020.assignment10;

import cz.janousek.adventofcode2020.AssignmentSolver;
import cz.janousek.adventofcode2020.LineReader;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class AssignmentSolver10 extends AssignmentSolver {

    private static final int START_SEQUENCE_BOUND = 0;
    private static final int END_SEQUENCE_BOUND_DIFFERENCE = 3;

    @Override
    public String solveFirstPart(final String inputFile) {
        final var numbers = LineReader.readResourceLines("input/" + inputFile).stream()
                .map(Integer::parseInt)
                .sorted()
                .collect(Collectors.toList());

        ArrayList<Integer> allNumbers = addBounds(numbers);

        int oneJoinSteps = 0;
        int threeJoinSteps = 0;
        for (int i = 1; i < allNumbers.size(); i++) {
            var actualJoltage = allNumbers.get(i);
            var previousJoltage = allNumbers.get(i-1);
            var jointageDifference = Math.abs(actualJoltage - previousJoltage);
            if (jointageDifference == 1) {
                oneJoinSteps++;
            } else if (jointageDifference == 3) {
                threeJoinSteps++;
            }
        }
        var result = oneJoinSteps * threeJoinSteps;

        return String.valueOf(result);
    }

    @Override
    public String solveSecondPart(final String inputFile) {
        final var numbers = LineReader.readResourceLines("input/" + inputFile).stream()
                .map(Integer::parseInt)
                .sorted()
                .collect(Collectors.toList());

        ArrayList<Integer> allNumbers = addBounds(numbers);
        List<List<Integer>> splitSequence = split(allNumbers);

        long result = splitSequence.stream()
                .map(sequence -> getPossibleArrangements(sequence, 1))
                .reduce(1L, (a, b) -> a * b);

        return String.valueOf(result);
    }

    private ArrayList<Integer> addBounds(List<Integer> numbers) {
        ArrayList<Integer> allNumbers = new ArrayList<>();
        allNumbers.add(START_SEQUENCE_BOUND);
        allNumbers.addAll(numbers);
        allNumbers.add(numbers.get(numbers.size() - 1) + END_SEQUENCE_BOUND_DIFFERENCE);
        return allNumbers;
    }

    private List<List<Integer>> split(List<Integer> numbers) {
        int previousEndIndex = 0;
        List<List<Integer>> result = new ArrayList<>();
        for (int i = 0; i < numbers.size() - 1; i++) {
            if (Math.abs(numbers.get(i) - numbers.get(i + 1)) > 2) {
                result.add(numbers.subList(previousEndIndex, i + 1));
                previousEndIndex = i + 1;
            }
        }
        result.add(numbers.subList(previousEndIndex, numbers.size()));
        return result;
    }

    private long getPossibleArrangements(List<Integer> allNumbers, int initialIndex) {
        int count = 1;
        for (int i = initialIndex; i < allNumbers.size() - 1; i++) {
            if (canRemove(allNumbers.get(i - 1), allNumbers.get(i + 1))) {
                var newArrangement = new ArrayList<>(allNumbers);
                newArrangement.remove(i);
                count += getPossibleArrangements(newArrangement, i);
            }
        }
        return count;
    }

    private boolean canRemove(int leftAdapter, int rightAdapter) {
        return Math.abs(leftAdapter - rightAdapter) <= 3;
    }

    @Override
    public String getInputFile() {
        return "input10.txt";
    }
}
