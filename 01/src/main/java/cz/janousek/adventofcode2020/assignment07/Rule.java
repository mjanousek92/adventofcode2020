package cz.janousek.adventofcode2020.assignment07;

import java.util.Arrays;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Rule {
    private static final String REGEX = "([1-9]+) (.*) bags?";
    private static final Pattern PATTERN = Pattern.compile(REGEX);

    private String bagName;
    private Set<RuleItem> items;

    public Rule(final String bagName, final Set<RuleItem> items) {
        this.bagName = bagName;
        this.items = items;
    }

    public String getBagName() {
        return bagName;
    }

    public Set<RuleItem> getItems() {
        return items;
    }

    public static Rule parseString(final String value) {
        final var trimmedLine = trimDotAtTheEndOfLine(value);
        final var conditionAndResult = trimmedLine.split(" bags contain ");
        final var condition = conditionAndResult[0];
        final var result = conditionAndResult[1];
        final var resultItems = result.split(", ");
        final var ruleItems = Arrays.stream(resultItems)
                .map(e -> parseRuleItem(e))
                .collect(Collectors.toSet());
        return new Rule(condition, ruleItems);
    }

    private static String trimDotAtTheEndOfLine(String value) {
        return value.substring(0, value.length() - 1);
    }

    private static RuleItem parseRuleItem(final String value) {
        if (value.equals("no other bags")) {
            return new RuleItem(0, "");
        }
        var matcher = PATTERN.matcher(value);
        matcher.matches();
        return new RuleItem(
                Integer.parseInt(matcher.group(1)),
                matcher.group(2));
    }

    public boolean containsAny(Set<String> outerBags) {
        return items.stream()
                .anyMatch(item -> outerBags.contains(item.getBag()));
    }

    public static class RuleItem {
        private int count;
        private String bag;

        public RuleItem(int count, String bag) {
            this.count = count;
            this.bag = bag;
        }

        public int getCount() {
            return count;
        }

        public String getBag() {
            return bag;
        }
    }
}
