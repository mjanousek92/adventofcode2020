package cz.janousek.adventofcode2020.assignment07;

import cz.janousek.adventofcode2020.AssignmentSolver;
import cz.janousek.adventofcode2020.LineReader;
import cz.janousek.adventofcode2020.assignment07.bag.Bag;
import cz.janousek.adventofcode2020.assignment07.bag.BagItem;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

public class AssignmentSolver07 extends AssignmentSolver {

    @Override
    public String solveFirstPart(final String inputFile) {
        final String myBag = "shiny gold";
        final var rules = LineReader.readResourceLines("input/" + inputFile).stream()
                .map(Rule::parseString)
                .collect(Collectors.toSet());

        var outerBags = computeOuterBags(myBag, rules);
        return String.valueOf(outerBags.size() - 1); // minus my bag
    }

    private Set<String> computeOuterBags(String myBag, Set<Rule> rules) {
        final Set<String> outerBags = new HashSet<>();
        outerBags.add(myBag);
        int outerBagCount;
        do {
            outerBagCount = outerBags.size();
            var newOuterBags = rules.stream()
                    .filter(r -> r.containsAny(outerBags))
                    .map(Rule::getBagName)
                    .collect(Collectors.toSet());
            outerBags.addAll(newOuterBags);
        } while ((outerBagCount != outerBags.size()));
        return outerBags;
    }

    @Override
    public String solveSecondPart(final String inputFile) {
        final String myBag = "shiny gold";
        final var rules = LineReader.readResourceLines("input/" + inputFile).stream()
                .map(Rule::parseString)
                .collect(Collectors.toSet());

        Map<String, Bag> bags = constructBagStructure(rules);
        var bagsCount = bags.get(myBag).getChildBagCount();
        return String.valueOf(bagsCount - 1); // minus my bag
    }

    private Map<String, Bag> constructBagStructure(Set<Rule> rules) {
        var rulesMap = rules.stream()
                .collect(Collectors.toMap(r -> r.getBagName(), Function.identity()));
        return constructBags(rulesMap);
    }

    private Map<String, Bag> constructBags(Map<String, Rule> rules) {
        Map<String, Bag> bags = new HashMap<>();
        rules.values().forEach(r -> bags.put(
                r.getBagName(),
                new Bag(r.getBagName())));
        rules.values().forEach(r -> appendBags(r, bags));
        return bags;
    }

    private void appendBags(Rule rule, Map<String, Bag> bags) {
        Bag bag = bags.get(rule.getBagName());
        rule.getItems().forEach(ruleItem -> {
                if (ruleItem.getCount() == 0) {
                    return;
                }
                bag.addBagItem(
                    new BagItem(
                        ruleItem.getCount(),
                        bags.get(ruleItem.getBag())));
        });
    }

    @Override
    public String getInputFile() {
        return "input07.txt";
    }
}
