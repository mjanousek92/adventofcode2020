package cz.janousek.adventofcode2020.assignment07.bag;

import java.util.HashSet;
import java.util.Set;

public class Bag {
    private final String name;
    private final Set<BagItem> innerBags;

    public Bag(final String name) {
        this.name = name;
        this.innerBags = new HashSet<>();
    }

    public String getName() {
        return name;
    }

    public void addBagItem(final BagItem item) {
        innerBags.add(item);
    }

    public Set<BagItem> getInnerBags() {
        return innerBags;
    }

    public int getChildBagCount() {
        return 1 + innerBags.stream()
                .mapToInt(ib -> ib.getCount() * ib.getBag().getChildBagCount())
                .sum();
    }
}
