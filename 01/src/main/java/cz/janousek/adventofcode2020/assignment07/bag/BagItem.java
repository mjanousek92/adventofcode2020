package cz.janousek.adventofcode2020.assignment07.bag;

public class BagItem {
    private final int count;
    private final Bag bag;

    public BagItem(final int count, final Bag bag) {
        this.count = count;
        this.bag = bag;
    }

    public int getCount() {
        return count;
    }

    public Bag getBag() {
        return bag;
    }
}
