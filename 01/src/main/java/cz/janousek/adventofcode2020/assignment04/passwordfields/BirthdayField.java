package cz.janousek.adventofcode2020.assignment04.passwordfields;

public class BirthdayField extends PasswordField {
    public BirthdayField(String value) {
        super(value);
    }

    @Override
    public boolean isValid() {
        int year = Integer.parseInt(value);
        return 1920 <= year && year <= 2002;
    }
}
