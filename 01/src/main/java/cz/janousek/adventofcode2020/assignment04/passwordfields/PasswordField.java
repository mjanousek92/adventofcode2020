package cz.janousek.adventofcode2020.assignment04.passwordfields;

public abstract class PasswordField {
    protected final String value;

    public PasswordField(String value) {
        this.value = value;
    }

    public abstract boolean isValid();
}
