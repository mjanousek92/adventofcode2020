package cz.janousek.adventofcode2020.assignment04.passwordfields;

public class HeightField extends PasswordField {
    public HeightField(String value) {
        super(value);
    }

    @Override
    public boolean isValid() {
        if (value.endsWith("cm")) {
            int height = Integer.parseInt(value.split("cm")[0]);
            return 150 <= height && height <= 193;
        } else if (value.endsWith("in")) {
            int height = Integer.parseInt(value.split("in")[0]);
            return 59 <= height && height <= 76;
        }
        return false;
    }
}
