package cz.janousek.adventofcode2020.assignment04.passwordfields;

public class CountryIdField extends PasswordField {
    public CountryIdField(String value) {
        super(value);
    }

    @Override
    public boolean isValid() {
        return true;
    }
}
