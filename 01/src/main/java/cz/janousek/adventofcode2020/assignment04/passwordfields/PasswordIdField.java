package cz.janousek.adventofcode2020.assignment04.passwordfields;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PasswordIdField extends PasswordField {
    final Pattern colorPattern = Pattern.compile("([0-9]{9})");

    public PasswordIdField(String value) {
        super(value);
    }

    @Override
    public boolean isValid() {
        Matcher matcher = colorPattern.matcher(value);
        return matcher.matches();
    }
}
