package cz.janousek.adventofcode2020.assignment04;

import cz.janousek.adventofcode2020.AssignmentSolver;
import cz.janousek.adventofcode2020.LineReader;
import org.springframework.util.StringUtils;

import java.util.LinkedList;
import java.util.List;

public class AssignmentSolver04 extends AssignmentSolver {

    @Override
    public String solveFirstPart(final String inputFile) {
        final var lines = LineReader.readResourceLines("input/" + inputFile);
        final LinkedList<String> passports = createOneLinePasswords(lines);

        var count = passports.stream()
                .filter(p -> StringUtils.countOccurrencesOf(p, ":") == 8 || (StringUtils.countOccurrencesOf(p, ":") == 7 && !p.contains("cid:")))
                .count();

        return String.valueOf(count);
    }

    @Override
    public String solveSecondPart(final String inputFile) {
        final var lines = LineReader.readResourceLines("input/" + inputFile);
        final LinkedList<String> passports = createOneLinePasswords(lines);

        var count = passports.stream()
                .map(PasswordDTO::parseString)
                .filter(p -> p.isValid())
                .count();

        return String.valueOf(count);
    }

    @Override
    public String getInputFile() {
        return "input04.txt";
    }

    private LinkedList<String> createOneLinePasswords(List<String> lines) {
        final LinkedList<String> passports = new LinkedList<>();
        String passport = "";
        for (String line : lines) {
            if (line.equals("") && passport != "") {
                passports.add(passport);
                passport = "";
            }
            passport = passport + " " + line;
        }
        if (!passport.equals("")) {
            passports.add(passport);
        }
        return passports;
    }
}
