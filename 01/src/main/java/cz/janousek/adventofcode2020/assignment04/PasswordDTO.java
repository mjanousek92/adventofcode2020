package cz.janousek.adventofcode2020.assignment04;

import cz.janousek.adventofcode2020.assignment04.passwordfields.CountryIdField;
import cz.janousek.adventofcode2020.assignment04.passwordfields.PasswordField;

import java.util.ArrayList;
import java.util.List;

public class PasswordDTO {
    final List<PasswordField> fields;

    public PasswordDTO() {
        fields = new ArrayList<>();
    }

    public void addField(final PasswordField passwordField) {
        fields.add(passwordField);
    }

    public static PasswordDTO parseString(String s) {
        final PasswordDTO passwordDTO = new PasswordDTO();
        final String[] arguments = s.split(" ");
        for (final String argument : arguments) {
            if (argument.isEmpty()) {
                continue;
            }
            passwordDTO.addField(PasswordFieldFactory.create(argument.split(":")));
        }
        return passwordDTO;
    }

    public boolean isValid() {
        if (fields.size() < 7 || (fields.size() == 7 && existCountryId())) {
            return false;
        }

        for (var field : fields) {
            if (!field.isValid()) {
                return false;
            }
        }
        return true;
    }

    private boolean existCountryId() {
        for (var field : fields) {
            if (field instanceof CountryIdField) {
                return true;
            }
        }
        return false;
    }
}
