package cz.janousek.adventofcode2020.assignment04.passwordfields;

public class IssueField extends PasswordField {
    public IssueField(String value) {
        super(value);
    }

    @Override
    public boolean isValid() {
        int year = Integer.parseInt(value);
        return 2010 <= year && year <= 2020;
    }
}
