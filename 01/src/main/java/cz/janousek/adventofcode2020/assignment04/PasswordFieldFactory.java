package cz.janousek.adventofcode2020.assignment04;

import cz.janousek.adventofcode2020.assignment04.passwordfields.*;

public class PasswordFieldFactory {
    public static PasswordField create(String[] argument) {

        if (argument[0].equals("byr")) {
            return new BirthdayField(argument[1]);
        } else if (argument[0].equals("iyr")) {
            return new IssueField(argument[1]);
        } else if (argument[0].equals("eyr")) {
            return new ExpirationField(argument[1]);
        } else if (argument[0].equals("hgt")) {
            return new HeightField(argument[1]);
        } else if (argument[0].equals("hcl")) {
            return new HairColorFieldField(argument[1]);
        } else if (argument[0].equals("ecl")) {
            return new EyeColorFieldField(argument[1]);
        } else if (argument[0].equals("pid")) {
            return new PasswordIdField(argument[1]);
        } else if (argument[0].equals("cid")) {
            return new CountryIdField(argument[1]);
        } else {
            throw new RuntimeException("Invalid input");
        }
    }
}
