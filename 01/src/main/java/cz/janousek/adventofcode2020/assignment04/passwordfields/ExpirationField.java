package cz.janousek.adventofcode2020.assignment04.passwordfields;

public class ExpirationField extends PasswordField {
    public ExpirationField(String value) {
        super(value);
    }

    @Override
    public boolean isValid() {
        int year = Integer.parseInt(value);
        return 2020 <= year && year <= 2030;
    }
}
