package cz.janousek.adventofcode2020.assignment04.passwordfields;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HairColorFieldField extends PasswordField {
    final Pattern colorPattern = Pattern.compile("#([0-9a-f]{6})");

    public HairColorFieldField(String value) {
        super(value);
    }

    @Override
    public boolean isValid() {
        Matcher matcher = colorPattern.matcher(value);
        return matcher.matches();
    }
}
