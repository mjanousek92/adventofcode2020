package cz.janousek.adventofcode2020.assignment04.passwordfields;

import java.util.List;

public class EyeColorFieldField extends PasswordField {
    private List<String> eyeColors = List.of("amb", "blu", "brn", "gry", "grn", "hzl", "oth");

    public EyeColorFieldField(String value) {
        super(value);
    }

    @Override
    public boolean isValid() {
        return eyeColors.contains(this.value);
    }
}
