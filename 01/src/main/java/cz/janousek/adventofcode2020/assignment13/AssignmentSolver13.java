package cz.janousek.adventofcode2020.assignment13;

import cz.janousek.adventofcode2020.AssignmentSolver;
import cz.janousek.adventofcode2020.LineReader;
import cz.janousek.adventofcode2020.assignment01.Tuple;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class AssignmentSolver13 extends AssignmentSolver {


    @Override
    public String solveFirstPart(final String inputFile) {
        final var lines = LineReader.readResourceLines("input/" + inputFile);
        final var timestamp = Integer.parseInt(lines.get(0));
        final var bussesStings = lines.get(1).split(",");

        final Tuple<Integer, Integer> bestbus = IntStream
                .range(0, bussesStings.length)
                .filter(i -> !bussesStings[i].equals("x"))
                .mapToObj(i -> new Tuple<>(i, Integer.parseInt(bussesStings[i])))
                .min(Comparator.comparing(bus -> getArrivesIn(bus, timestamp)))
                .get();

        return String.valueOf(bestbus.getSecondItem() * getArrivesIn(bestbus, timestamp));
    }

    public int getArrivesIn(final Tuple<Integer, Integer> bus, final int timestamp) {
        int times = timestamp / bus.getSecondItem();
        return bus.getSecondItem() * (times + 1) - timestamp;
    }

    @Override
    public String solveSecondPart(final String inputFile) {
        final var lines = LineReader.readResourceLines("input/" + inputFile);
        final var bussesStrings = lines.get(1).split(",");
        final List<Tuple<Integer, Integer>> busses = IntStream
                .range(0, bussesStrings.length)
                .filter(i -> !bussesStrings[i].equals("x"))
                .mapToObj(i -> new Tuple<>(i, Integer.parseInt(bussesStrings[i])))
                .collect(Collectors.toList());

        sortBussesByIdDesc(busses);

        long timestamp = busses.get(0).getSecondItem() - busses.get(0).getFirstItem();
        long period = busses.get(0).getSecondItem();
        for (int busIndex = 1; busIndex < busses.size() ; busIndex++) {
            boolean isTimestampSatisfyingForBusses = true;
            while (isTimestampSatisfyingForBusses)
            {
                final long finalTimestamp = timestamp;
                final var bussesSublist = busses.subList(0, busIndex+1);
                isTimestampSatisfyingForBusses = bussesSublist.stream()
                        .anyMatch(t -> ((finalTimestamp + t.getFirstItem()) % t.getSecondItem()) != 0);

                if (isTimestampSatisfyingForBusses) {
                    timestamp += period;
                }
            }

            period = getNextPeriod(busses, busIndex);
        }

        return String.valueOf(timestamp);
    }

    private Long getNextPeriod(List<Tuple<Integer, Integer>> busses, int busIndex) {
        return busses.subList(0, busIndex + 1).stream()
                .map(t -> (long) t.getSecondItem())
                .reduce(1L, this::lcm);
    }

    private void sortBussesByIdDesc(List<Tuple<Integer, Integer>> busses) {
        busses.sort(
                Comparator
                        .comparing(Tuple<Integer, Integer>::getSecondItem)
                        .reversed());
    }

    public long lcm(long number1, long number2) {
        if (number1 == 0 || number2 == 0) {
            return 0;
        }
        long higherNumber = Math.max(number1, number2);
        long lowerNumber = Math.min(number1, number2);
        long lcm = higherNumber;
        while (lcm % lowerNumber != 0) {
            lcm += higherNumber;
        }
        return lcm;
    }

    @Override
    public String getInputFile() {
        return "input13.txt";
    }
}
