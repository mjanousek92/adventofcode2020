package cz.janousek.adventofcode2020.assignment01;

import cz.janousek.adventofcode2020.LineReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.stream.Collectors;

public class AssignmentSolver01 {
    private static final Logger LOGGER = LoggerFactory.getLogger(LineReader.class);

    public static void main(String[] args) {
        final AssignmentSolver01 assignmentSolver = new AssignmentSolver01();
        assignmentSolver.solveFirstPart();
        assignmentSolver.solveSecondPart();
    }

    private void solveFirstPart() {
        final var lines = LineReader.readResourceLines("input/input01.txt").stream()
                .map(Integer::parseInt)
                .collect(Collectors.toList());
        final var tuple = NumberFinder.findTuple(lines, (a, b) -> a + b == 2020);
        if (tuple == null ){
            LOGGER.info("Numbers are not found.");
            return;
        }

        var result = tuple.getFirstItem() * tuple.getSecondItem();
        LOGGER.info("Numbers found! {} * {} = {}", tuple.getFirstItem(), tuple.getSecondItem(), result);
    }

    private void solveSecondPart() {
        final var lines = LineReader.readResourceLines("input/input01.txt").stream()
                .map(Integer::parseInt)
                .collect(Collectors.toList());
        final var numbers = NumberFinder.find3(lines, (a, b, c) -> a + b + c == 2020);
        if (numbers == null ){
            LOGGER.info("Numbers are not found.");
            return;
        }

        var result = numbers.stream().reduce(1, (a, b) -> a * b);
        LOGGER.info("Numbers found! Result of numbers {} is {}", numbers, result);
    }
}
