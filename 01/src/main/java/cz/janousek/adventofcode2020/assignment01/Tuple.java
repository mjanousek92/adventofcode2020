package cz.janousek.adventofcode2020.assignment01;

import java.util.Objects;

public class Tuple<T, U> {
    private final T firstItem;
    private final U secondItem;

    public Tuple(final T firstItem, final U secondItem) {
        this.firstItem = firstItem;
        this.secondItem = secondItem;
    }

    public T getFirstItem() {
        return firstItem;
    }

    public U getSecondItem() {
        return secondItem;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tuple<?, ?> tuple = (Tuple<?, ?>) o;
        return Objects.equals(firstItem, tuple.firstItem) &&
                Objects.equals(secondItem, tuple.secondItem);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstItem, secondItem);
    }
}
