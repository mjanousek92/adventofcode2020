package cz.janousek.adventofcode2020.assignment01;

import java.util.List;
import java.util.function.BiPredicate;

public class NumberFinder {
    public static <T> Tuple<T, T> findTuple(final List<T> collection, final BiPredicate<T, T> predicate) {
        for (int i = 0; i < collection.size(); i++) {
            final T firstElement = collection.get(i);
            for (int j = i + 1; j < collection.size(); j++) {
                final T secondElement = collection.get(j);
                if (predicate.test(firstElement, secondElement)) {
                    return new Tuple<>(firstElement, secondElement);
                }
            }
        }
        return null;
    }

    public static <T> List<T> find3(final List<T> collection, final TriPredicate<T, T, T> predicate) {
        for (int i = 0; i < collection.size(); i++) {
            final T firstElement = collection.get(i);
            for (int j = i + 1; j < collection.size(); j++) {
                final T secondElement = collection.get(j);
                for (int k = i + 1; k < collection.size(); k++) {
                    final T thirdElement = collection.get(k);
                    if (predicate.test(firstElement, secondElement, thirdElement)) {
                        return List.of(firstElement, secondElement, thirdElement);
                    }
                }
            }
        }
        return null;
    }
}
