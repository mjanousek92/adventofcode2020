package cz.janousek.adventofcode2020.assignment01;

@FunctionalInterface
public interface TriPredicate<T, U, V> {
    boolean test(T t, T u, T v);
}
