package cz.janousek.adventofcode2020.assignment12;

import cz.janousek.adventofcode2020.AssignmentSolver;
import cz.janousek.adventofcode2020.LineReader;

import java.util.stream.Collectors;

public class AssignmentSolver12 extends AssignmentSolver {

    private static final int START_SEQUENCE_BOUND = 0;
    private static final int END_SEQUENCE_BOUND_DIFFERENCE = 3;

    @Override
    public String solveFirstPart(final String inputFile) {
        final var instructions = LineReader.readResourceLines("input/" + inputFile).stream()
                .map(InstructionFactory::parseString)
                .collect(Collectors.toList());

        final Navigation navigation = new Navigation(instructions);
        int result = navigation.getManhattanDistance();

        return String.valueOf(result);
    }

    @Override
    public String solveSecondPart(final String inputFile) {
        final var instructions = LineReader.readResourceLines("input/" + inputFile).stream()
                .map(InstructionFactory::parseString)
                .collect(Collectors.toList());

        final Navigation navigation = new Navigation(instructions);
        int result = navigation.getManhattanDistance();

        return String.valueOf(result);
    }

    @Override
    public String getInputFile() {
        return "input12.txt";
    }
}
