package cz.janousek.adventofcode2020.assignment12.instruction;

import cz.janousek.adventofcode2020.assignment12.Navigation;

public class WestInstruction extends Instruction {
    public WestInstruction(int value) {
        super(value);
    }

    @Override
    public void apply(Navigation navigation) {
        navigation.getWaypoint().addWest(getValue());
    }
}
