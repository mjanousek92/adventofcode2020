package cz.janousek.adventofcode2020.assignment12;

import cz.janousek.adventofcode2020.assignment12.instruction.*;

public class InstructionFactory {
    public static Instruction parseString(final String input) {
        var instruction = input.substring(0,1);
        var value = Integer.parseInt(input.substring(1, input.length()));

        if (instruction.equals("N")) {
            return new NorthInstruction(value);
        } else if (instruction.equals("S")) {
            return new SouthInstruction(value);
        } if (instruction.equals("E")) {
            return new EastInstruction(value);
        } if (instruction.equals("W")) {
            return new WestInstruction(value);
        } if (instruction.equals("F")) {
            return new ForwardInstruction(value);
        } if (instruction.equals("L")) {
            return new LeftInstruction(value);
        } if (instruction.equals("R")) {
            return new RightInstruction(value);
        }
        throw new IllegalArgumentException();
    }
}
