package cz.janousek.adventofcode2020.assignment12.instruction;

import cz.janousek.adventofcode2020.assignment12.Direction;
import cz.janousek.adventofcode2020.assignment12.Navigation;
import cz.janousek.adventofcode2020.assignment12.Position;

public abstract class Instruction {

    private final int value;

    public Instruction(final int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public abstract void apply(Navigation navigation);
}
