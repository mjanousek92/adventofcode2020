package cz.janousek.adventofcode2020.assignment12;

public enum Direction {
    NORTH{
        @Override
        public Direction rotate(int rotation) {
            rotation = (rotation + 360) % 360;
            if (rotation == 0) {
                return NORTH;
            } else if (rotation == 90) {
                return EAST;
            } else if (rotation == 180) {
                return SOUTH;
            } else if (rotation == 270) {
                return WEST;
            } else {
                throw new IllegalArgumentException(String.valueOf(rotation));
            }
        }
    },
    EAST{
        @Override
        public Direction rotate(int rotation) {
            rotation = (rotation + 360) % 360;
            if (rotation == 0) {
                return EAST;
            } else if (rotation == 90) {
                return SOUTH;
            } else if (rotation == 180) {
                return WEST;
            } else if (rotation == 270) {
                return NORTH;
            } else {
                throw new IllegalArgumentException(String.valueOf(rotation));
            }
        }
    },
    SOUTH{
        @Override
        public Direction rotate(int rotation) {
            rotation = (rotation + 360) % 360;
            if (rotation == 0) {
                return SOUTH;
            } else if (rotation == 90) {
                return WEST;
            } else if (rotation == 180) {
                return NORTH;
            } else if (rotation == 270) {
                return EAST;
            } else {
                throw new IllegalArgumentException(String.valueOf(rotation));
            }
        }
    },
    WEST{
        @Override
        public Direction rotate(int rotation) {
            rotation = (rotation + 360) % 360;
            if (rotation == 0) {
                return WEST;
            } else if (rotation == 90) {
                return NORTH;
            } else if (rotation == 180) {
                return EAST;
            } else if (rotation == 270) {
                return SOUTH;
            } else {
                throw new IllegalArgumentException(String.valueOf(rotation));
            }
        }
    };

    public abstract Direction rotate(int rotation);
}
