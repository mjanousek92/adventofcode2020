package cz.janousek.adventofcode2020.assignment12.instruction;

import cz.janousek.adventofcode2020.assignment12.Direction;
import cz.janousek.adventofcode2020.assignment12.Navigation;
import cz.janousek.adventofcode2020.assignment12.Position;

public class NorthInstruction extends Instruction {
    public NorthInstruction(int value) {
        super(value);
    }

    @Override
    public void apply(Navigation navigation) {
        navigation.getWaypoint().addNorth(getValue());
    }
}
