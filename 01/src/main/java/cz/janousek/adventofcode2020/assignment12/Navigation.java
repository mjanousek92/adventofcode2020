package cz.janousek.adventofcode2020.assignment12;

import cz.janousek.adventofcode2020.assignment12.instruction.Instruction;

import java.util.List;

public class Navigation {
    private Direction direction = Direction.EAST;
    private Position position = new Position(0, 0, 0, 0);
    private Position waypoint = new Position(1, 10, 0, 0);
    private final List<Instruction> instruction;

    public Navigation(List<Instruction> instructions) {
        this.instruction = instructions;
        navigate();
    }

    private void navigate() {
        instruction.forEach(i -> i.apply(this));
    }

    public int getManhattanDistance() {
        return position.getManhattanDistance();
    }

    public Direction getDirection() {
        return direction;
    }

    public Position getPosition() {
        return position;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    public Position getWaypoint() {
        return waypoint;
    }
}
