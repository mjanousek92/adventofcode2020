package cz.janousek.adventofcode2020.assignment12.instruction;

import cz.janousek.adventofcode2020.assignment12.Direction;
import cz.janousek.adventofcode2020.assignment12.Navigation;
import cz.janousek.adventofcode2020.assignment12.Position;

public class ForwardInstruction extends Instruction {
    public ForwardInstruction(int value) {
        super(value);
    }

    @Override
    public void apply(Navigation navigation) {
        var waypoint = navigation.getWaypoint();

//        if (direction == Direction.NORTH) {
//            navigation.getPosition().addNorth(getValue());
//        } else if (direction == Direction.EAST) {
//            navigation.getPosition().addEast(getValue());
//        } else if (direction == Direction.SOUTH) {
//            navigation.getPosition().addSouth(getValue());
//        } else if (direction == Direction.WEST) {
//            navigation.getPosition().addWest(getValue());
//        } else {
//            throw new IllegalArgumentException();
//        }

        navigation.getPosition().addNorth(waypoint.getNorth() * getValue());
        navigation.getPosition().addEast(waypoint.getEast() * getValue());
        navigation.getPosition().addSouth(waypoint.getSouth() * getValue());
        navigation.getPosition().addWest(waypoint.getWest() * getValue());
    }
}
