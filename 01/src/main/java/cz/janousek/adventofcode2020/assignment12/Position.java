package cz.janousek.adventofcode2020.assignment12;

public class Position {
    private int north;
    private int east;
    private int south;
    private int west;

    public Position(int north, int east, int south, int west) {
        this.north = north;
        this.east = east;
        this.south = south;
        this.west = west;
    }

    public int getNorth() {
        return north;
    }

    public void addNorth(int north) {
        this.north += north;
    }

    public int getEast() {
        return east;
    }

    public void addEast(int east) {
        this.east += east;
    }

    public int getSouth() {
        return south;
    }

    public void addSouth(int south) {
        this.south += south;
    }

    public int getWest() {
        return west;
    }

    public void addWest(int west) {
        this.west += west;
    }

    public int getManhattanDistance() {
        return Math.abs(east - west) + Math.abs(north - south);
    }

    public void rotateRight(int value) {
        value = (value + 360) % 360;
        int north = this.north;
        int east = this.east;
        int south = this.south;
        int west = this.west;

        if (value == 0) {
            // do nothing
        } else if (value == 90) {
            this.north = west;
            this.east = north;
            this.south = east;
            this.west = south;
        } else if (value == 180) {
            this.north = south;
            this.east = west;
            this.south = north;
            this.west = east;
        } else if (value == 270) {
            this.north = east;
            this.east = south;
            this.south = west;
            this.west = north;
        } else {
            throw new IllegalArgumentException(String.valueOf(value));
        }
    }
}
