package cz.janousek.adventofcode2020.assignment12.instruction;

import cz.janousek.adventofcode2020.assignment12.Navigation;
import cz.janousek.adventofcode2020.assignment12.Position;

public class RightInstruction extends Instruction {
    public RightInstruction(int value) {
        super(value);
    }

    @Override
    public void apply(Navigation navigation) {
        Position waypoint = navigation.getWaypoint();
        waypoint.rotateRight(getValue());
    }
}
