package cz.janousek.adventofcode2020.assignment12.instruction;

import cz.janousek.adventofcode2020.assignment12.Direction;
import cz.janousek.adventofcode2020.assignment12.Navigation;
import cz.janousek.adventofcode2020.assignment12.Position;

public class SouthInstruction extends Instruction {
    public SouthInstruction(int value) {
        super(value);
    }

    @Override
    public void apply(Navigation navigation) {
        navigation.getWaypoint().addSouth(getValue());
    }
}
