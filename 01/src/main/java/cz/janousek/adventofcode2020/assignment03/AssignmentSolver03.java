package cz.janousek.adventofcode2020.assignment03;

import cz.janousek.adventofcode2020.AssignmentSolver;
import cz.janousek.adventofcode2020.LineReader;

import java.util.List;

public class AssignmentSolver03 extends AssignmentSolver {

    @Override
    public String solveFirstPart(final String inputFile) {
        final var lines = LineReader.readResourceLines("input/" + inputFile);
        int treeCount = countTreesInLines(lines, 1, 3);
        return String.valueOf(treeCount);
    }

    @Override
    public String solveSecondPart(final String inputFile) {
        final var lines = LineReader.readResourceLines("input/" + inputFile);
        int treeCount = 1;
        treeCount *= countTreesInLines(lines, 1, 1);
        treeCount *= countTreesInLines(lines, 1, 3);
        treeCount *= countTreesInLines(lines, 1, 5);
        treeCount *= countTreesInLines(lines, 1, 7);
        treeCount *= countTreesInLines(lines, 2, 1);
        return String.valueOf(treeCount);
    }

    private int countTreesInLines(List<String> lines, final int lineStep, final int columnStep) {
        int position = 0;
        int treeCount = 0;
        for (int i = 0; i < lines.size(); i = i + lineStep) {
            final String line = lines.get(i);
            if (isTreeAtPosition(line, position)) {
                treeCount++;
            }
            position = (position + columnStep) % line.length();
        }
        return treeCount;
    }

    private boolean isTreeAtPosition(String line, int position) {
        return line.charAt(position) == '#';
    }

    @Override
    public String getInputFile() {
        return "input03.txt";
    }
}
