package cz.janousek.adventofcode2020.assignment06;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class Group {
    final Set<Character> distinctPositiveAnswers;

    public Group(List<String> group) {
        distinctPositiveAnswers = group.stream()
                .flatMap(s -> s.chars().mapToObj(c -> (char) c))
                .collect(Collectors.toSet());
    }

    public int getDistinctPositiveAnswersCount() {
        return distinctPositiveAnswers.size();
    }
}
