package cz.janousek.adventofcode2020.assignment06;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Group2 {
    final Set<Character> distinctPositiveAnswers;

    public Group2(List<String> group) {
        List<Set<Character>> listOfSets = group.stream()
                .map(s -> s.chars()
                        .mapToObj(c -> (char) c)
                        .collect(Collectors.toSet()))
                .collect(Collectors.toList());

        distinctPositiveAnswers = listOfSets.stream()
                .skip(1)
                .collect(()->new HashSet<>(listOfSets.get(0)), Set::retainAll, Set::retainAll);

    }

    public int getDistinctPositiveAnswersCount() {
        return distinctPositiveAnswers.size();
    }
}
