package cz.janousek.adventofcode2020.assignment06;

import cz.janousek.adventofcode2020.AssignmentSolver;
import cz.janousek.adventofcode2020.LineReader;
import cz.janousek.adventofcode2020.assignment05.PassportDTO;

import java.util.ArrayList;
import java.util.List;

public class AssignmentSolver06 extends AssignmentSolver {

    @Override
    public String solveFirstPart(final String inputFile) {
        final var lines = LineReader.readResourceLines("input/" + inputFile);
        List<String> group = new ArrayList<>();
        List<Group> groups = new ArrayList<>();
        for (final String line : lines) {
            if (line.isEmpty()) {
                groups.add(new Group(group));
                group.clear();
            } else {
                group.add(line);
            }
        }
        if (!group.isEmpty()) {
            groups.add(new Group(group));
        }

        int sum = groups.stream()
                .map(g -> g.getDistinctPositiveAnswersCount())
                .mapToInt(Integer::intValue)
                .sum();
        return String.valueOf(sum);
    }

    @Override
    public String solveSecondPart(final String inputFile) {
        final var lines = LineReader.readResourceLines("input/" + inputFile);
        List<String> group = new ArrayList<>();
        List<Group2> groups = new ArrayList<>();
        for (final String line : lines) {
            if (line.isEmpty()) {
                groups.add(new Group2(group));
                group.clear();
            } else {
                group.add(line);
            }
        }
        if (!group.isEmpty()) {
            groups.add(new Group2(group));
        }

        int sum = groups.stream()
                .map(g -> g.getDistinctPositiveAnswersCount())
                .mapToInt(Integer::intValue)
                .sum();
        return String.valueOf(sum);
    }

    @Override
    public String getInputFile() {
        return "input06.txt";
    }
}
