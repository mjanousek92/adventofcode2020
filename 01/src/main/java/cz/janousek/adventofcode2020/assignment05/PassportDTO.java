package cz.janousek.adventofcode2020.assignment05;

public class PassportDTO {
    private int row;
    private int column;

    public PassportDTO(int row, int column) {
        this.row = row;
        this.column = column;
    }

    public int getRow() {
        return row;
    }

    public int getColumn() {
        return column;
    }

    public static PassportDTO parseString(final String input) {
        final String binaryString = input
                .replaceAll("F", "0")
                .replaceAll("B", "1")
                .replaceAll("R", "1")
                .replaceAll("L", "0");
        final String binaryRow = binaryString.substring(0, 7);
        final String binaryColumn = binaryString.substring(7, 10);

        return new PassportDTO(
                Integer.parseInt(binaryRow, 2),
                Integer.parseInt(binaryColumn, 2));
    }

    public int countSeatId() {
        return row * 8 + column;
    }
}
