package cz.janousek.adventofcode2020.assignment05;

import cz.janousek.adventofcode2020.AssignmentSolver;
import cz.janousek.adventofcode2020.LineReader;

import java.util.*;
import java.util.stream.Collectors;

public class AssignmentSolver05 extends AssignmentSolver {

    // F = 0, B = 1
    // R = 1, L = 0

    @Override
    public String solveFirstPart(final String inputFile) {
        final var maxSeatID = LineReader.readResourceLines("input/" + inputFile).stream()
                .map(PassportDTO::parseString)
                .map(PassportDTO::countSeatId)
                .max(Integer::compareTo)
                .get();
        return String.valueOf(maxSeatID);
    }

    @Override
    public String solveSecondPart(final String inputFile) {
        final boolean[][] freeSeats = generateAllSeats();

        LineReader.readResourceLines("input/" + inputFile).stream()
                .map(PassportDTO::parseString)
                .forEach(p -> freeSeats[p.getRow()][p.getColumn()] = false);

        for (int i = 0; i < freeSeats.length; i++) {
            var row = freeSeats[i];
            if (isNotEmptyRow(row) && isNotFullRow(row) && existFreeSeatInTheMiddle(row)) {
                final PassportDTO passportDTO = new PassportDTO(i, getEmptyFieldSeatNumber(row));
                return String.valueOf(passportDTO.countSeatId());
            }
        }
        return "";
    }

    @Override
    public String getInputFile() {
        return "input05.txt";
    }

    private int getEmptyFieldSeatNumber(boolean[] seats) {
        for (int i = 1; i < seats.length - 1; i++) {
            if (seats[i] && !seats[i-1] && !seats[i+1]) {
                return i;
            }
        }
        return -1;
    }

    private boolean existFreeSeatInTheMiddle(boolean[] seats) {
        for (int i = 1; i < seats.length - 1; i++) {
            if (seats[i] && !seats[i-1] && !seats[i+1]) {
                return true;
            }
        }
        return false;
    }

    private boolean isNotEmptyRow(boolean[] seats) {
        for (var seat : seats) {
            if (seat) {
                return true;
            }
        }
        return false;
    }

    private boolean isNotFullRow(boolean[] seats) {
        for (var seat : seats) {
            if (!seat) {
                return true;
            }
        }
        return false;
    }

    private boolean[][] generateAllSeats() {
        var seats = new boolean[128][8];
        for (int i = 0; i < 128; i++) {
            for (int j = 0; j < 8; j++) {
                seats[i][j] = true;
            }
        }
        return seats;
    }
}
