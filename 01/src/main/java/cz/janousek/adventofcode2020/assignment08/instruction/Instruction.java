package cz.janousek.adventofcode2020.assignment08.instruction;

public abstract class Instruction {
    protected final int argument;

    public Instruction(int argumnet) {
        this.argument = argumnet;
    }

    public abstract int getNextLine(int actualLine);
    public abstract int getNextAcumulatorValue(int actualAccumulator);

    public int getArgument() {
        return argument;
    }
}
