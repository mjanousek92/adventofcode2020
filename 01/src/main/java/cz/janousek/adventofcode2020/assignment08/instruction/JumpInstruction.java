package cz.janousek.adventofcode2020.assignment08.instruction;

public class JumpInstruction extends Instruction {
    public JumpInstruction(final int argumnet) {
        super(argumnet);
    }

    @Override
    public int getNextLine(int actualLine) {
        return actualLine + argument;
    }

    @Override
    public int getNextAcumulatorValue(int actualAccumulator) {
        return actualAccumulator;
    }
}
