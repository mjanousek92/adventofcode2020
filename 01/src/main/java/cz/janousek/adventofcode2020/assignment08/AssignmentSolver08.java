package cz.janousek.adventofcode2020.assignment08;

import cz.janousek.adventofcode2020.AssignmentSolver;
import cz.janousek.adventofcode2020.LineReader;
import cz.janousek.adventofcode2020.assignment01.Tuple;
import cz.janousek.adventofcode2020.assignment07.Rule;
import cz.janousek.adventofcode2020.assignment07.bag.Bag;
import cz.janousek.adventofcode2020.assignment07.bag.BagItem;
import cz.janousek.adventofcode2020.assignment08.instruction.Instruction;
import cz.janousek.adventofcode2020.assignment08.instruction.JumpInstruction;
import cz.janousek.adventofcode2020.assignment08.instruction.NoOperationInstruction;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class AssignmentSolver08 extends AssignmentSolver {

    @Override
    public String solveFirstPart(final String inputFile) {
        final var instructions = LineReader.readResourceLines("input/" + inputFile)
                .stream()
                .map(InstructionFactory::create)
                .collect(Collectors.toUnmodifiableList());
        final Program program = new Program(instructions);
        final int result = program.run().getFirstItem();

        return String.valueOf(result);
    }

    @Override
    public String solveSecondPart(final String inputFile) {
        final var instructions = LineReader.readResourceLines("input/" + inputFile)
                .stream()
                .map(InstructionFactory::create)
                .collect(Collectors.toList());

        Tuple<Integer, Boolean> result = null;
        for (int i = 0; i < instructions.size(); i++) {
            var runInstructions = new ArrayList<>(instructions);
            var instruction = runInstructions.get(i);
            var newInstruction = getNewInstruction(instruction);
            runInstructions.set(i, newInstruction);
            final Program program = new Program(runInstructions);
            result = program.run();
            if (result.getSecondItem()) {
                break;
            }
        }

        return String.valueOf(result.getFirstItem());
    }

    private Instruction getNewInstruction(Instruction instruction) {
        if (instruction instanceof NoOperationInstruction) {
            return new JumpInstruction(instruction.getArgument());
        } else if (instruction instanceof JumpInstruction) {
            return new NoOperationInstruction(instruction.getArgument());
        }
        return instruction;
    }

    @Override
    public String getInputFile() {
        return "input08.txt";
    }
}
