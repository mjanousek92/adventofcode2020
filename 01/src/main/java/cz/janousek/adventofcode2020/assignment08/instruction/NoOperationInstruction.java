package cz.janousek.adventofcode2020.assignment08.instruction;

public class NoOperationInstruction extends Instruction {
    public NoOperationInstruction(final int argument) {
        super(argument);
    }

    @Override
    public int getNextLine(int actualLine) {
        return actualLine + 1;
    }

    @Override
    public int getNextAcumulatorValue(int actualAccumulator) {
        return actualAccumulator;
    }
}
