package cz.janousek.adventofcode2020.assignment08;

import cz.janousek.adventofcode2020.assignment08.instruction.AccInstruction;
import cz.janousek.adventofcode2020.assignment08.instruction.Instruction;
import cz.janousek.adventofcode2020.assignment08.instruction.JumpInstruction;
import cz.janousek.adventofcode2020.assignment08.instruction.NoOperationInstruction;

public class InstructionFactory {
    public static Instruction create(String input) {
        var instructionAndArgument = input.split(" ");
        var instruction = instructionAndArgument[0];
        var argument = Integer.parseInt(instructionAndArgument[1]);
        switch (instruction) {
            case "jmp":
                return new JumpInstruction(argument);
            case "acc":
                return new AccInstruction(argument);
            default:
                return new NoOperationInstruction(argument);
        }
    }
}
