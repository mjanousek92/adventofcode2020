package cz.janousek.adventofcode2020.assignment08;

import cz.janousek.adventofcode2020.assignment01.Tuple;
import cz.janousek.adventofcode2020.assignment08.instruction.Instruction;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Program {
    private final List<Instruction> instructions;
    private int acumulator = 0;
    private int statememtPointer = 0;
    private Set<Integer> executedStatements = new HashSet<>();

    public
    Program(List<Instruction> instructions) {
        this.instructions = instructions;
    }

    public Tuple<Integer, Boolean> run() {
        do {
            executedStatements.add(statememtPointer);
            var instruction = instructions.get(statememtPointer);
            acumulator = instruction.getNextAcumulatorValue(acumulator);
            statememtPointer = instruction.getNextLine(statememtPointer);
        } while (!isTerminated() && !executedStatements.contains(statememtPointer));
        return new Tuple<>(acumulator, isTerminated());
    }

    private boolean isTerminated() {
        return statememtPointer == instructions.size();
    }
}
