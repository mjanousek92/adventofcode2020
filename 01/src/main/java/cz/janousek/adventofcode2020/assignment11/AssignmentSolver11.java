package cz.janousek.adventofcode2020.assignment11;

import cz.janousek.adventofcode2020.AssignmentSolver;
import cz.janousek.adventofcode2020.LineReader;

import java.util.List;
import java.util.stream.Collectors;

public class AssignmentSolver11 extends AssignmentSolver {

    private static final int START_SEQUENCE_BOUND = 0;
    private static final int END_SEQUENCE_BOUND_DIFFERENCE = 3;

    @Override
    public String solveFirstPart(final String inputFile) {
        final List<char[]> initialSeat = LineReader.readResourceLines("input/" + inputFile).stream()
                .map(String::toCharArray)
                .collect(Collectors.toList());

        char[][] oldSeatingPlan = new char[initialSeat.size()][initialSeat.get(0).length];
        for (int i = 0; i < initialSeat.size(); i++) {
            oldSeatingPlan[i] = initialSeat.get(i);
        }
        char[][] newSeatingPlan = copy(oldSeatingPlan);
        do {
            oldSeatingPlan = newSeatingPlan;
            newSeatingPlan = seatPeople(oldSeatingPlan);
        } while (isDifferent(oldSeatingPlan, newSeatingPlan));

        int result = countOccupiedSeats(newSeatingPlan);
        return String.valueOf(result);
    }

    private int countOccupiedSeats(char[][] newSeatingPlan) {
        int count = 0;
        for (int row = 0; row < newSeatingPlan.length; row++) {
            for (int column = 0; column < newSeatingPlan[row].length; column++) {
                if (newSeatingPlan[row][column] == '#') {
                    count++;
                }
            }
        }
        return count;
    }

    public boolean isDifferent(char[][] firstSeatingPlan, char[][] secondSeatingPlan) {
        for (int row = 0; row < firstSeatingPlan.length; row++) {
            for (int column = 0; column < firstSeatingPlan[row].length; column++) {
                if (firstSeatingPlan[row][column] != secondSeatingPlan[row][column]) {
                    return true;
                }
            }
        }
        return false;
    }

    private char[][] seatPeople(char[][] oldSeatingPlan) {
        char[][] newSeatingPlan = copy(oldSeatingPlan);

        for (int rowIndex = 0; rowIndex < oldSeatingPlan.length; rowIndex++) { // rows
            char[] row = oldSeatingPlan[rowIndex];
            for (int column = 0; column < row.length; column++) { // columns
                int occupiedAdjectedSeat = getOcupiedAdjectedSeatsAround(oldSeatingPlan, rowIndex, column);
                if (oldSeatingPlan[rowIndex][column] == 'L' && occupiedAdjectedSeat == 0) {
                    newSeatingPlan[rowIndex][column] = '#';
                }

                if (oldSeatingPlan[rowIndex][column] == '#' && occupiedAdjectedSeat >= 4) {
                    newSeatingPlan[rowIndex][column] = 'L';
                }
            }
        }

        return newSeatingPlan;
    }

    private int getOcupiedAdjectedSeatsAround(char[][] seatingPlan, int row, int column) {
        int occupiedAdjectedSeatsAround = 0;
        if (isOccupiedSeat(seatingPlan, row + 1, column)) {
            occupiedAdjectedSeatsAround++;
        }
        if (isOccupiedSeat(seatingPlan, row + 1, column + 1)) {
            occupiedAdjectedSeatsAround++;
        }
        if (isOccupiedSeat(seatingPlan, row, column + 1)) {
            occupiedAdjectedSeatsAround++;
        }
        if (isOccupiedSeat(seatingPlan, row - 1, column)) {
            occupiedAdjectedSeatsAround++;
        }
        if (isOccupiedSeat(seatingPlan, row - 1, column - 1)) {
            occupiedAdjectedSeatsAround++;
        }
        if (isOccupiedSeat(seatingPlan, row, column - 1)) {
            occupiedAdjectedSeatsAround++;
        }
        if (isOccupiedSeat(seatingPlan, row + 1, column - 1)) {
            occupiedAdjectedSeatsAround++;
        }
        if (isOccupiedSeat(seatingPlan, row - 1, column + 1)) {
            occupiedAdjectedSeatsAround++;
        }
        return occupiedAdjectedSeatsAround;
    }

    private boolean isOccupiedSeat(char[][] seatingPlan, int row, int column) {
        return row >= 0
                && row < seatingPlan.length
                && column >= 0
                && column < seatingPlan[row].length
                && seatingPlan[row][column] == '#';
    }

    public char[][] copy(char[][] input) {
        return java.util.Arrays.stream(input).map(el -> el.clone()).toArray($ -> input.clone());
    }

    @Override
    public String solveSecondPart(final String inputFile) {
        final List<char[]> initialSeat = LineReader.readResourceLines("input/" + inputFile).stream()
                .map(String::toCharArray)
                .collect(Collectors.toList());

        char[][] oldSeatingPlan = new char[initialSeat.size()][initialSeat.get(0).length];
        for (int i = 0; i < initialSeat.size(); i++) {
            oldSeatingPlan[i] = initialSeat.get(i);
        }
        char[][] newSeatingPlan = copy(oldSeatingPlan);
        do {
            oldSeatingPlan = newSeatingPlan;
            newSeatingPlan = seatPeople2(oldSeatingPlan);
        } while (isDifferent(oldSeatingPlan, newSeatingPlan));

        int result = countOccupiedSeats(newSeatingPlan);
        return String.valueOf(result);
    }

    private char[][] seatPeople2(char[][] oldSeatingPlan) {
        char[][] newSeatingPlan = copy(oldSeatingPlan);

        for (int rowIndex = 0; rowIndex < oldSeatingPlan.length; rowIndex++) { // rows
            char[] row = oldSeatingPlan[rowIndex];
            for (int column = 0; column < row.length; column++) { // columns
                int occupiedAdjectedSeat = getVisibleOcupiedAdjectedSeatsAround(oldSeatingPlan, rowIndex, column);
                if (oldSeatingPlan[rowIndex][column] == 'L' && occupiedAdjectedSeat == 0) {
                    newSeatingPlan[rowIndex][column] = '#';
                }

                if (oldSeatingPlan[rowIndex][column] == '#' && occupiedAdjectedSeat >= 5) {
                    newSeatingPlan[rowIndex][column] = 'L';
                }
            }
        }

        return newSeatingPlan;
    }

    private int getVisibleOcupiedAdjectedSeatsAround(char[][] seatingPlan, int row, int column) {
        int occupiedAdjectedSeatsAround = 0;
        if (isOccupiedSeatInDirection(seatingPlan, row, column, 1, 0)) {
            occupiedAdjectedSeatsAround++;
        }
        if (isOccupiedSeatInDirection(seatingPlan, row, column, 1, 1)) {
            occupiedAdjectedSeatsAround++;
        }
        if (isOccupiedSeatInDirection(seatingPlan, row, column, 0, 1)) {
            occupiedAdjectedSeatsAround++;
        }
        if (isOccupiedSeatInDirection(seatingPlan, row, column, -1, 0)) {
            occupiedAdjectedSeatsAround++;
        }
        if (isOccupiedSeatInDirection(seatingPlan, row, column, -1, -1)) {
            occupiedAdjectedSeatsAround++;
        }
        if (isOccupiedSeatInDirection(seatingPlan, row, column, 0, -1)) {
            occupiedAdjectedSeatsAround++;
        }
        if (isOccupiedSeatInDirection(seatingPlan, row, column, 1, -1)) {
            occupiedAdjectedSeatsAround++;
        }
        if (isOccupiedSeatInDirection(seatingPlan, row, column, -1, 1)) {
            occupiedAdjectedSeatsAround++;
        }
        return occupiedAdjectedSeatsAround;
    }

    private boolean isOccupiedSeatInDirection(char[][] seatingPlan, int row, int column, int rowDirection, int columnDirection) {
        int rowIndex = row + rowDirection;
        int columnIndex = column + columnDirection;
        while (rowIndex >= 0
                && rowIndex < seatingPlan.length
                && columnIndex >= 0
                && columnIndex < seatingPlan[rowIndex].length) {
            if (seatingPlan[rowIndex][columnIndex] == '#') {
                return true;
            }
            if (seatingPlan[rowIndex][columnIndex] == 'L') {
                return false;
            }
            rowIndex += rowDirection;
            columnIndex += columnDirection;
        }
        return false;
    }

    @Override
    public String getInputFile() {
        return "input11.txt";
    }
}
