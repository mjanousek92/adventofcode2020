package cz.janousek.adventofcode2020;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import java.io.IOException;
import java.nio.file.Files;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class LineReader {
    private static final Logger LOGGER = LoggerFactory.getLogger(LineReader.class);

    public static List<String> readResourceLines(final String input) {
        final Resource resource = new ClassPathResource(input);
        try (Stream<String> stream = Files.lines(resource.getFile().toPath())) {
            return stream.collect(Collectors.toList());
        } catch (IOException e) {
            LOGGER.error("Cannot read file.", e);
        }
        return Collections.emptyList();
    }
}
