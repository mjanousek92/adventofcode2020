package cz.janousek.adventofcode2020.assignment02;

public class PasswordWithNewPolicyDTO extends PasswordWithPolicyDTO {
    public PasswordWithNewPolicyDTO(int minimumOccurrence, int maximumOccurrence, char character, String password) {
        super(minimumOccurrence, maximumOccurrence, character, password);
    }

    @Override
    public boolean isValid() {
        return (isCharacterAtPosition(getMinimumOccurrence()) && !isCharacterAtPosition(getMaximumOccurrence()))
                ||  (!isCharacterAtPosition(getMinimumOccurrence()) && isCharacterAtPosition(getMaximumOccurrence()));
    }

    public static PasswordWithPolicyDTO parseString(String s) {
        var constraintAndPassword = s.split(": ");
        final String password = constraintAndPassword[1];
        var occurrencesAndCharacter = constraintAndPassword[0].split(" ");
        final String character = occurrencesAndCharacter[1];
        var occurrences = occurrencesAndCharacter[0].split("-");
        final String minimumOccurrence = occurrences[0];
        final String maximumOccurrence = occurrences[1];
        return new PasswordWithNewPolicyDTO(
                Integer.parseInt(minimumOccurrence),
                Integer.parseInt(maximumOccurrence),
                character.charAt(0),
                password);

    }

    private boolean isCharacterAtPosition(final int position) {
        var zeroBasedPositoin = position - 1;
        if (zeroBasedPositoin >= getPassword().length()) {
            return false;
        }
        return getPassword().charAt(zeroBasedPositoin) == getCharacter();
    }
}
