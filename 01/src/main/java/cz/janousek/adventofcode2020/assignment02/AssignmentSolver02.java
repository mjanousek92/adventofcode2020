package cz.janousek.adventofcode2020.assignment02;

import cz.janousek.adventofcode2020.AssignmentSolver;
import cz.janousek.adventofcode2020.LineReader;

public class AssignmentSolver02 extends AssignmentSolver {

    @Override
    public String solveFirstPart(final String inputFile) {
        final var count = LineReader.readResourceLines("input/" + inputFile).stream()
                .map(PasswordWithPolicyDTO::parseString)
                .filter(PasswordWithPolicyDTO::isValid)
                .count();
        return Long.toString(count);
    }

    @Override
    public String solveSecondPart(final String inputFile) {
        final var count = LineReader.readResourceLines("input/" + inputFile).stream()
                .map(s -> PasswordWithNewPolicyDTO.parseString(s))
                .filter(PasswordWithPolicyDTO::isValid)
                .count();
        return Long.toString(count);
    }

    @Override
    public String getInputFile() {
        return "input02.txt";
    }
}
