package cz.janousek.adventofcode2020.assignment02;

import org.springframework.util.StringUtils;

import java.util.Objects;

public class PasswordWithPolicyDTO {
    private int minimumOccurrence;
    private int maximumOccurrence;
    private char character;
    private String password;

    public PasswordWithPolicyDTO(int minimumOccurrence, int maximumOccurrence, char character, String password) {
        this.minimumOccurrence = minimumOccurrence;
        this.maximumOccurrence = maximumOccurrence;
        this.character = character;
        this.password = password;
    }

    public int getMinimumOccurrence() {
        return minimumOccurrence;
    }

    public int getMaximumOccurrence() {
        return maximumOccurrence;
    }

    public char getCharacter() {
        return character;
    }

    public String getPassword() {
        return password;
    }

    public static PasswordWithPolicyDTO parseString(String s) {
        var constraintAndPassword = s.split(": ");
        final String password = constraintAndPassword[1];
        var occurrencesAndCharacter = constraintAndPassword[0].split(" ");
        final String character = occurrencesAndCharacter[1];
        var occurrences = occurrencesAndCharacter[0].split("-");
        final String minimumOccurrence = occurrences[0];
        final String maximumOccurrence = occurrences[1];
        return new PasswordWithPolicyDTO(
                Integer.parseInt(minimumOccurrence),
                Integer.parseInt(maximumOccurrence),
                character.charAt(0),
                password);

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PasswordWithPolicyDTO that = (PasswordWithPolicyDTO) o;
        return minimumOccurrence == that.minimumOccurrence &&
                maximumOccurrence == that.maximumOccurrence &&
                Objects.equals(character, that.character) &&
                Objects.equals(password, that.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(minimumOccurrence, maximumOccurrence, character, password);
    }

    public boolean isValid() {
        int occurrences = StringUtils.countOccurrencesOf(password, String.valueOf(character));
        return minimumOccurrence <= occurrences && occurrences <= maximumOccurrence;
    }
}
