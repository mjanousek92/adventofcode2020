package cz.janousek.adventofcode2020.assignment14;

public class BitInstructionFactory {
    public static BitInstruction parseString(final String input) {
        var instructionAndValue = input.split(" = ");
        if (instructionAndValue[0].equals("mask")) {
            return new MaskBitInstruction(instructionAndValue[1]);
        } else if (instructionAndValue[0].startsWith("mem")) {
            var address = instructionAndValue[0].substring(4, instructionAndValue[0].length() - 1);
            return new MemoryBitInstruction(instructionAndValue[1], address);
        } else {
            throw new IllegalArgumentException(input);
        }
    }
}
