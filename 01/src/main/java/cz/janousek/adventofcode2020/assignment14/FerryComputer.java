package cz.janousek.adventofcode2020.assignment14;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FerryComputer {
    private final List<BitInstruction> instruction;
    private BitInstruction actualMask;
    private final Map<Long, Long> memory;

    public FerryComputer(List<BitInstruction> instructions) {
        this.instruction = instructions;
        memory = new HashMap<>();
    }

    public void compute() {
        instruction.forEach(i -> i.execute(this));
    }

    public BitInstruction getActualMask() {
        return actualMask;
    }


    public void setActualMask(BitInstruction actualMask) {
        this.actualMask = actualMask;
    }

    public void setMemorry(long address, long numericValue) {
        memory.put(address, numericValue);
    }

    public long getSumOfMemory() {
        return memory.values().stream()
                .mapToLong(Long::longValue)
                .sum();
    }
}
