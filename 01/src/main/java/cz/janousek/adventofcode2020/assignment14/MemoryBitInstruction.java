package cz.janousek.adventofcode2020.assignment14;

import com.google.common.collect.Sets;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MemoryBitInstruction extends BitInstruction {
    private final String address;

    public MemoryBitInstruction(String value, String address) {
        super(value);
        this.address = address;
    }

    private static String getCreateBitString(String value) {
        String binaryValue = Integer.toUnsignedString(Integer.parseInt(value), 2);
        binaryValue = addPadding(binaryValue);
        return binaryValue;
    }

    private static String addPadding(String binaryValue) {
        int size = binaryValue.length();
        String prefix = "0".repeat(36 - size);
        return prefix + binaryValue;
    }

    @Override
    public void execute(FerryComputer ferryComputer) {
        var mask = ferryComputer.getActualMask();
        var bitAddressString = getCreateBitString(address);
        var stringAddress = applyMask(mask, bitAddressString);
        List<Long> addresses = getAddresses(stringAddress);
        addresses.forEach(a -> ferryComputer.setMemorry(a, Integer.parseInt(getValue())));
    }

    private List<Long> getAddresses(String stringAddress) {
        List<Integer> xBits = getBitPossitions(stringAddress, 'X');
        List<Long> addresses = new ArrayList<>();

        String baseAddress = setMaskBits(stringAddress, xBits, '0');
        addresses.add(Long.parseLong(baseAddress, 2));

        for (int i = 1; i <= xBits.size(); i++) {
            Set<Set<Integer>> positions = getPositionCombinations(xBits, i);
            positions.forEach(combination ->
                    addresses.add(Long.parseLong(setMaskBits(baseAddress, new ArrayList<>(combination), '1'), 2)));
        }

        return addresses;
    }

    private Set<Set<Integer>> getPositionCombinations(List<Integer> possiblePositions, int count) {
        return Sets.combinations(new HashSet<>(possiblePositions), count);
    }

    private long getNumericValue() {
        return Long.parseLong(getValue(), 2);
    }

    private String applyMask(BitInstruction mask, String input) {
//        List<Integer> zeroBits = getBitPossitions(mask.getValue(), '0');
        List<Integer> oneBits = getBitPossitions(mask.getValue(), '1');
        List<Integer> xBits = getBitPossitions(mask.getValue(), 'X');
//        String newValue = setMaskBits(input, zeroBits, '0');
        String newValue = setMaskBits(input, oneBits, '1');
        newValue = setMaskBits(newValue, xBits, 'X');
        return newValue;
    }

    private String setMaskBits(String string, List<Integer> zeroBits, char c) {
        char[] chars = string.toCharArray();
        for (var bit : zeroBits) {
            chars[bit] = c;
        }
        return String.valueOf(chars);
    }

    private List<Integer> getBitPossitions(String input, char guess) {
        List<Integer> result = new ArrayList<>();
        int index = input.indexOf(guess);
        while (index >= 0) {
            result.add(index);
            index = input.indexOf(guess, index + 1);
        }
        return result;
    }
}
