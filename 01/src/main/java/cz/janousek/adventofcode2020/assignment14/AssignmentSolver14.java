package cz.janousek.adventofcode2020.assignment14;

import cz.janousek.adventofcode2020.AssignmentSolver;
import cz.janousek.adventofcode2020.LineReader;

import java.util.stream.Collectors;

public class AssignmentSolver14 extends AssignmentSolver {


    @Override
    public String solveFirstPart(final String inputFile) {
        final var instructions = LineReader.readResourceLines("input/" + inputFile).stream()
                .map(BitInstructionFactory::parseString)
                .collect(Collectors.toList());

        FerryComputer computer = new FerryComputer(instructions);
        computer.compute();
        long result = computer.getSumOfMemory();

        return String.valueOf(result);
    }

    @Override
    public String solveSecondPart(final String inputFile) {
        final var instructions = LineReader.readResourceLines("input/" + inputFile).stream()
                .map(BitInstructionFactory::parseString)
                .collect(Collectors.toList());

        FerryComputer computer = new FerryComputer(instructions);
        computer.compute();
        long result = computer.getSumOfMemory();

        return String.valueOf(result);
    }

    @Override
    public String getInputFile() {
        return "input14.txt";
    }
}
