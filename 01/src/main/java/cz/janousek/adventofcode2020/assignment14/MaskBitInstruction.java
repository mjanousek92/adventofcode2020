package cz.janousek.adventofcode2020.assignment14;

public class MaskBitInstruction extends BitInstruction {
    public MaskBitInstruction(String value) {
        super(value);
    }

    @Override
    public void execute(FerryComputer ferryComputer) {
        ferryComputer.setActualMask(this);
    }
}
