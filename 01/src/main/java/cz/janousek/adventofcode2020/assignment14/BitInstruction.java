package cz.janousek.adventofcode2020.assignment14;

public abstract class BitInstruction {
    private String value;

    public BitInstruction(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    protected void setValue(String value) {
        this.value = value;
    }

    public abstract void execute(FerryComputer ferryComputer);
}
