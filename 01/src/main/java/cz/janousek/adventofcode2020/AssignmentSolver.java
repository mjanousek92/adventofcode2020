package cz.janousek.adventofcode2020;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class AssignmentSolver {
    private static final Logger LOGGER = LoggerFactory.getLogger(AssignmentSolver.class);

    public abstract String solveFirstPart(String inputFile);
    public abstract String solveSecondPart(String inputFile);
    public abstract String getInputFile();

    public void solve() {
        LOGGER.info("Result of 1st part is {}", solveFirstPart(getInputFile()));
        LOGGER.info("Result of 1nd part is {}", solveSecondPart(getInputFile()));
    }
}
