package cz.janousek.adventofcode2020.assignment09;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

public class XmasChecker {
    private final List<BigDecimal> numbers;

    public XmasChecker(List<BigDecimal> numbers) {
        this.numbers = numbers;
    }

    public BigDecimal getCorruptedNumber(int preamble) {
        for (int i = preamble; i < numbers.size(); i++) {
            var actualNumber = numbers.get(i);
            if (!isSumFromPreamble(actualNumber, numbers.subList(i - preamble, i))) {
                return actualNumber;
            }
        }
        return null;
    }

    private boolean isSumFromPreamble(BigDecimal actualNumber, List<BigDecimal> preambleNumbers) {
        for (int i = 0; i < preambleNumbers.size(); i++) {
            var firstNumber = preambleNumbers.get(i);
            for (int j = i + 1; j < preambleNumbers.size(); j++) {
                var secondNumber = preambleNumbers.get(j);
                if (firstNumber.add(secondNumber).equals(actualNumber)) {
                    return true;
                }
            }
        }
        return false;
    }

    public BigDecimal getEncryptionWeakness(BigDecimal corruptedNumber) {
        int corruptedNumberIndex = numbers.indexOf(corruptedNumber);
        int previousNumberRange = 2;
        for (int i = 0; i < corruptedNumberIndex - 2; i++) {
            BigDecimal previousNumberSum = numbers.get(corruptedNumberIndex - i - 1).add(numbers.get(corruptedNumberIndex - i - 2));
            while (previousNumberSum.compareTo(corruptedNumber) < 0) {
                previousNumberRange++;
                previousNumberSum = previousNumberSum.add(numbers.get(corruptedNumberIndex - i - previousNumberRange));
            }
            if (previousNumberSum.compareTo(corruptedNumber) == 0) {
                var sublist = numbers.subList(corruptedNumberIndex - i - previousNumberRange, corruptedNumberIndex - i);
                sublist = sublist.stream()
                        .sorted()
                        .collect(Collectors.toList());
                return sublist.get(sublist.size() - 1).add(sublist.get(0));
            }
            previousNumberRange = 2;
        }
        return null;
    }
}
