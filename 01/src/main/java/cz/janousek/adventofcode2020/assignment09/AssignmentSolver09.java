package cz.janousek.adventofcode2020.assignment09;

import cz.janousek.adventofcode2020.AssignmentSolver;
import cz.janousek.adventofcode2020.LineReader;

import java.math.BigDecimal;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class AssignmentSolver09 extends AssignmentSolver {

    @Override
    public String solveFirstPart(final String inputFile) {
        final List<BigDecimal> numbers = LineReader.readResourceLines("input/" + inputFile).stream()
                .map(n -> new BigDecimal(n))
                .collect(Collectors.toList());
        XmasChecker xmasChecker = new XmasChecker(numbers);
        var corruptedNumber = xmasChecker.getCorruptedNumber(25);
//                .stream()
//                .map(InstructionFactory::create)
//                .collect(Collectors.toUnmodifiableList());
//        final Program program = new Program(instructions);
//        final int result = program.run().getFirstItem();

        return String.valueOf(corruptedNumber);
    }

    @Override
    public String solveSecondPart(final String inputFile) {
        final List<BigDecimal> numbers = LineReader.readResourceLines("input/" + inputFile).stream()
                .map(n -> new BigDecimal(n))
                .collect(Collectors.toList());
        XmasChecker xmasChecker = new XmasChecker(numbers);
        var corruptedNumber = xmasChecker.getCorruptedNumber(25);
        var encryptionWeakness = xmasChecker.getEncryptionWeakness(corruptedNumber);

        return String.valueOf(encryptionWeakness);
    }

    @Override
    public String getInputFile() {
        return "input09.txt";
    }
}
