package cz.janousek.adventofcode2020.assignment09;

import cz.janousek.adventofcode2020.AssignmentSolver;
import cz.janousek.adventofcode2020.AssignmentSolverBaseTest;
import cz.janousek.adventofcode2020.assignment08.AssignmentSolver08;

class AssignmentSolver09Test extends AssignmentSolverBaseTest {

    @Override
    protected AssignmentSolver createAssignmentSolver() {
        return new AssignmentSolver09();
    }

    @Override
    protected String getFistAssignmentResult() {
        return "127";
    }

    @Override
    protected String getSecondAssignmentResult() {
        return "62";
    }
}