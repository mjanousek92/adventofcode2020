package cz.janousek.adventofcode2020.assignment10;

import cz.janousek.adventofcode2020.AssignmentSolver;
import cz.janousek.adventofcode2020.AssignmentSolverBaseTest;
import cz.janousek.adventofcode2020.assignment09.AssignmentSolver09;

class AssignmentSolver10Test extends AssignmentSolverBaseTest {

    @Override
    protected AssignmentSolver createAssignmentSolver() {
        return new AssignmentSolver10();
    }

    @Override
    protected String getFistAssignmentResult() {
        return "a";
    }

    @Override
    protected String getSecondAssignmentResult() {
        return "19208";
    }
}