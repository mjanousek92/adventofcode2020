package cz.janousek.adventofcode2020.assignment02;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class PasswordWithPolicyDTOTest {

    @Test
    void parseStringTest() {
        var passwordWithContraint = PasswordWithPolicyDTO.parseString("1-5 f: aajjok");

        var expected = new PasswordWithPolicyDTO(1, 5, 'f', "aajjok");
        assertThat(passwordWithContraint).isEqualTo(expected);
    }
}