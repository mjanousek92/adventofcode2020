package cz.janousek.adventofcode2020.assignment02;

import cz.janousek.adventofcode2020.AssignmentSolver;
import cz.janousek.adventofcode2020.AssignmentSolverBaseTest;

class AssignmentSolver02Test extends AssignmentSolverBaseTest {

    @Override
    protected AssignmentSolver createAssignmentSolver() {
        return new AssignmentSolver02();
    }

    @Override
    protected String getFistAssignmentResult() {
        return "2";
    }

    @Override
    protected String getSecondAssignmentResult() {
        return "1";
    }
}