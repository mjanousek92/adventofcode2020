package cz.janousek.adventofcode2020.assignment04;

import cz.janousek.adventofcode2020.AssignmentSolver;
import cz.janousek.adventofcode2020.AssignmentSolverBaseTest;
import cz.janousek.adventofcode2020.assignment03.AssignmentSolver03;

class AssignmentSolver04Test extends AssignmentSolverBaseTest {

    @Override
    protected AssignmentSolver createAssignmentSolver() {
        return new AssignmentSolver04();
    }

    @Override
    protected String getFistAssignmentResult() {
        return "2";
    }

    @Override
    protected String getSecondAssignmentResult() {
        return "4";
    }
}