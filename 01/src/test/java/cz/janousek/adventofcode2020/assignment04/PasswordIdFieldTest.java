package cz.janousek.adventofcode2020.assignment04;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class PasswordIdFieldTest {
    @Test
    public void test() {
        var p = PasswordFieldFactory.create("byr:2002".split(":"));
        assertThat(p.isValid()).isTrue();
    }

    @Test
    public void test2() {
        var p = PasswordFieldFactory.create("byr:2003".split(":"));
        assertThat(p.isValid()).isFalse();
    }

    @Test
    public void test3() {

        var p = PasswordFieldFactory.create("hgt:60in".split(":"));
        assertThat(p.isValid()).isTrue();
    }

    @Test
    public void test4() {
        var p = PasswordFieldFactory.create("hgt:190cm".split(":"));
        assertThat(p.isValid()).isTrue();
    }

    @Test
    public void test5() {
        var p = PasswordFieldFactory.create("hgt:190in".split(":"));
        assertThat(p.isValid()).isFalse();
    }

    @Test
    public void test6() {
        var p = PasswordFieldFactory.create("hgt:190".split(":"));
        assertThat(p.isValid()).isFalse();
    }

    @Test
    public void test7() {

        var p = PasswordFieldFactory.create("hcl:#123abc".split(":"));
        assertThat(p.isValid()).isTrue();
    }

    @Test
    public void test8() {
        var p = PasswordFieldFactory.create("hcl:#123abz".split(":"));
        assertThat(p.isValid()).isFalse();
    }

    @Test
    public void test9() {
        var p = PasswordFieldFactory.create("hcl:123abc".split(":"));
        assertThat(p.isValid()).isFalse();
    }

    @Test
    public void test10() {
        var p = PasswordFieldFactory.create("ecl:brn".split(":"));
        assertThat(p.isValid()).isTrue();
    }

    @Test
    public void test11() {
        var p = PasswordFieldFactory.create("ecl:wat".split(":"));
        assertThat(p.isValid()).isFalse();
    }

    @Test
    public void test12() {
        var p = PasswordFieldFactory.create("pid:000000001".split(":"));
        assertThat(p.isValid()).isTrue();
    }

    @Test
    public void test13() {
        var p = PasswordFieldFactory.create("pid:0123456789".split(":"));
        assertThat(p.isValid()).isFalse();
    }
}