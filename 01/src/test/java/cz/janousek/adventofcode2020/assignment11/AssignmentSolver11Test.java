package cz.janousek.adventofcode2020.assignment11;

import cz.janousek.adventofcode2020.AssignmentSolver;
import cz.janousek.adventofcode2020.AssignmentSolverBaseTest;
import cz.janousek.adventofcode2020.assignment10.AssignmentSolver10;

class AssignmentSolver11Test extends AssignmentSolverBaseTest {

    @Override
    protected AssignmentSolver createAssignmentSolver() {
        return new AssignmentSolver11();
    }

    @Override
    protected String getFistAssignmentResult() {
        return "37";
    }

    @Override
    protected String getSecondAssignmentResult() {
        return "26";
    }
}