package cz.janousek.adventofcode2020.assignment01;

import cz.janousek.adventofcode2020.assignment01.NumberFinder;
import cz.janousek.adventofcode2020.assignment01.Tuple;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class NumberFinderTest {

    @Test
    void findTupleWithSumOf10() {
        var numbers = List.of(5,2,3,8,9,8,9);
        var actual = NumberFinder.findTuple(numbers, (a, b) -> a + b == 10);
        assertThat(actual).isEqualTo(new Tuple<>(2, 8));
    }

    @Test
    void findTupleWithSumOf10IfNotExist() {
        var numbers = List.of(5,6,3,8,9,8,9);
        var actual = NumberFinder.findTuple(numbers, (a, b) -> a + b == 10);
        assertThat(actual).isNull();
    }

    @Test
    void find3WithSumOf10() {
        var numbers = List.of(5,2,3,8,9,8,9);
        var actual = NumberFinder.find3(numbers, (a, b, c) -> a + b + c  == 20);
        assertThat(actual).isNotNull();
    }
}