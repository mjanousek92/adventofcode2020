package cz.janousek.adventofcode2020.assignment14;

import cz.janousek.adventofcode2020.AssignmentSolver;
import cz.janousek.adventofcode2020.AssignmentSolverBaseTest;

class AssignmentSolver14Test extends AssignmentSolverBaseTest {

    @Override
    protected AssignmentSolver createAssignmentSolver() {
        return new AssignmentSolver14();
    }

    @Override
    protected String getFistAssignmentResult() {
        return "165";
    }

    @Override
    protected String getSecondAssignmentResult() {
        return "nothing";
    }
}