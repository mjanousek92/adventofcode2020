package cz.janousek.adventofcode2020;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public abstract class AssignmentSolverBaseTest {
    private AssignmentSolver assignmentSolver;

    protected abstract AssignmentSolver createAssignmentSolver();
    protected abstract String getFistAssignmentResult();
    protected abstract String getSecondAssignmentResult();

    public AssignmentSolver getAssignmentSolver() {
        return assignmentSolver;
    }

    @BeforeEach
    void init() {
        assignmentSolver = createAssignmentSolver();
    }

    @Test
    void solveFirstPart() {
        var actual = getAssignmentSolver().solveFirstPart("test/" + assignmentSolver.getInputFile());
        assertThat(actual).isEqualTo(getFistAssignmentResult());
    }

    @Test
    void solveSecondPart() {
        var actual = getAssignmentSolver().solveSecondPart("test/" + assignmentSolver.getInputFile());
        assertThat(actual).isEqualTo(getSecondAssignmentResult());
    }

    @Test
    void testSolve() {
        getAssignmentSolver().solve();
    }
}
