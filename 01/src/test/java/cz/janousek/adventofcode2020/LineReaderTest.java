package cz.janousek.adventofcode2020;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class LineReaderTest {
    @Test
    void readInputFromResourceTest() {
        var lines = LineReader.readResourceLines("input/input01.txt");
        assertThat(lines).hasSize(200);
    }
}