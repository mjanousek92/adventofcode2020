package cz.janousek.adventofcode2020.assignment13;

import cz.janousek.adventofcode2020.AssignmentSolver;
import cz.janousek.adventofcode2020.AssignmentSolverBaseTest;
import cz.janousek.adventofcode2020.assignment12.AssignmentSolver12;

class AssignmentSolver13Test extends AssignmentSolverBaseTest {

    @Override
    protected AssignmentSolver createAssignmentSolver() {
        return new AssignmentSolver13();
    }

    @Override
    protected String getFistAssignmentResult() {
        return "295";
    }

    @Override
    protected String getSecondAssignmentResult() {
        return "1068781";
    }
}