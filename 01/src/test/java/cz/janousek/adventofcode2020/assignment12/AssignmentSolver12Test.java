package cz.janousek.adventofcode2020.assignment12;

import cz.janousek.adventofcode2020.AssignmentSolver;
import cz.janousek.adventofcode2020.AssignmentSolverBaseTest;
import cz.janousek.adventofcode2020.assignment11.AssignmentSolver11;

class AssignmentSolver12Test extends AssignmentSolverBaseTest {

    @Override
    protected AssignmentSolver createAssignmentSolver() {
        return new AssignmentSolver12();
    }

    @Override
    protected String getFistAssignmentResult() {
        return "25";
    }

    @Override
    protected String getSecondAssignmentResult() {
        return "286";
    }
}