package cz.janousek.adventofcode2020.assignment07;

import cz.janousek.adventofcode2020.AssignmentSolver;
import cz.janousek.adventofcode2020.AssignmentSolverBaseTest;

class AssignmentSolver07Test extends AssignmentSolverBaseTest {

    @Override
    protected AssignmentSolver createAssignmentSolver() {
        return new AssignmentSolver07();
    }

    @Override
    protected String getFistAssignmentResult() {
        return "4";
    }

    @Override
    protected String getSecondAssignmentResult() {
        return "126";
    }
}