package cz.janousek.adventofcode2020.assignment03;

import cz.janousek.adventofcode2020.AssignmentSolver;
import cz.janousek.adventofcode2020.AssignmentSolverBaseTest;

class AssignmentSolver03Test extends AssignmentSolverBaseTest {

    @Override
    protected AssignmentSolver createAssignmentSolver() {
        return new AssignmentSolver03();
    }

    @Override
    protected String getFistAssignmentResult() {
        return "7";
    }

    @Override
    protected String getSecondAssignmentResult() {
        return "336";
    }
}