package cz.janousek.adventofcode2020.assignment05;

import cz.janousek.adventofcode2020.AssignmentSolver;
import cz.janousek.adventofcode2020.AssignmentSolverBaseTest;
import cz.janousek.adventofcode2020.assignment04.AssignmentSolver04;

class AssignmentSolver05Test extends AssignmentSolverBaseTest {

    @Override
    protected AssignmentSolver createAssignmentSolver() {
        return new AssignmentSolver05();
    }

    @Override
    protected String getFistAssignmentResult() {
        return "820";
    }

    @Override
    protected String getSecondAssignmentResult() {
        return "";
    }
}