package cz.janousek.adventofcode2020.assignment06;

import cz.janousek.adventofcode2020.AssignmentSolver;
import cz.janousek.adventofcode2020.AssignmentSolverBaseTest;

class AssignmentSolver06Test extends AssignmentSolverBaseTest {

    @Override
    protected AssignmentSolver createAssignmentSolver() {
        return new AssignmentSolver06();
    }

    @Override
    protected String getFistAssignmentResult() {
        return "11";
    }

    @Override
    protected String getSecondAssignmentResult() {
        return "6";
    }
}