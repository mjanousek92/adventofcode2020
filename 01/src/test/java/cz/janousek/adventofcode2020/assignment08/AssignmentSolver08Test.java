package cz.janousek.adventofcode2020.assignment08;

import cz.janousek.adventofcode2020.AssignmentSolver;
import cz.janousek.adventofcode2020.AssignmentSolverBaseTest;
import cz.janousek.adventofcode2020.assignment07.AssignmentSolver07;

class AssignmentSolver08Test extends AssignmentSolverBaseTest {

    @Override
    protected AssignmentSolver createAssignmentSolver() {
        return new AssignmentSolver08();
    }

    @Override
    protected String getFistAssignmentResult() {
        return "5";
    }

    @Override
    protected String getSecondAssignmentResult() {
        return "8";
    }
}